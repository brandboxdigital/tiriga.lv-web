import { writable } from 'svelte/store'

let hash = location.hash.substr(1)
let selLang = hash.split('/')[0]
if( !['lat','rus','eng'].includes( selLang ) ) selLang = 'lat'

import lat from './lat.js'
import rus from './rus.js'
import eng from './eng.js'

const languages = { lat , rus , eng }

export const changeLanguage = (newLang) => {      
  let hashLang = newLang.split('/')[0]
  hashLang = hashLang == '' ? 'lat' : hashLang

  subLang.set( languages[hashLang] )
}
export const subLang = writable( languages[selLang] )

export const lang = languages[selLang]
export default {
  'lang':'lat',

  'start':'Sākums',

  // header
  header:{
    'lottery':'Loterija',
    'infograph':'Infografika',  
  },  
  'news':'Jaunumi',
  'faq':'Jautājumi',
  'contacts':'Kontakti',
  'contractSamples':'Līguma paraugi',
  'privatePerson':'Privātpersonām',
  'legalPerson':'Juridiskām personām',

  'makeadeal':'Noslēdz līgumu',

  'callme':'<b>Tālrunis:</b><br />67847844,<br />1848*',

  'setLangLAT':'LAT',
  'setLangRUS':'RUS',
  'setLangENG':'ENG',

  'faqTitle':'JAUTĀJUMI UN ATBILDES<br />“TĪRĪGA” AS sadzīves atkritumu apsaimniekošanas sistēmas ieviešana Rīgas pilsētā',

  // intro
  'ourcity':'mūsu pilsēta<br />ir',
  'obey':'Aicinām savlaicīgi pārslēgt līgumu par atkritumu apsaimniekošanu, lai Tīrīga var parūpēties par tavu pilsētu.',

  "title":"Rīga kļūst<br />tīrīga!",
  "intro":'Tīrīga” AS tika izveidota, lai publiskās-privātās partnerības ietvaros Rīgā veiktu atkritumu apsaimniekošanas sistēmas modernizāciju un ieviestu mūsdienīgu, efektīvu un iedzīvotājiem ērtu atkritumu šķirošanas sistēmu un risinājumus, tostarp attīstot infrastruktūru un nodrošinot iespēju iedzīvotājiem šķirot atkritumus. Sadzīves atkritumu apsaimniekošanas pakalpojumu nodrošināšana Rīgā tiks uzsākta no 2019.gada 15.septembra privātās-publiskās partnerības ietvaros saskaņā ar pašvaldības publiskā iepirkuma rezultātiem.',
  "gotext":'Tu pārslēdz līgumu šodien, un mēs – <br />“Tīrīga” AS – izdarīsim pārējo!',

  'actionChange':'PĀRSLĒGT LĪGUMU',

  // why
  'whythis':'Kāpēc Tīrīga?',
  'whyintro':'Ir pienācis laiks modernizēt Rīgas sadzīves atkritumu apsaimniekošanu. SIA “Getliņi EKO” un SIA "CREB Rīga" reģistrētā “Tīrīga” AS aizsāk vērienīgāko Rīgas pilsētas atkritumu apsaimniekošanas modernizācijas projektu ar vienotu attīstības mērķi – lai Rīga kļūtu tīrīga.',

  // recycling
  recycling:{
    'menu':'Kā pareizi šķirot?',
    'title':'Kā pareizi šķirot?',
    household:{
      'h2':'Sadzīves atkritumi',
      'h3':'Nedrīkst mest:',
      'li1':'<li>- lielgabarīta atkritumus</li>'+
            '<li>- riepas</li>'+
            '<li>- spuldzes un baterijas</li>'+
            '<li>- būvgružus</li>'+
            '<li>- elektrotehniku</li>',
      'li2':'<li>- medicīniska rakstura atkritumus</li>'+
            '<li>- karstus pelnus</li>'+
            '<li>- radioaktīvas vielas</li>'+
            '<li>- šķidros atkritumus</li>',

    },
    paper:{
      'h2':'Papīrs, plastmasa un metāls',
      'h3':'Nedrīkst mest:',
      'p':'Dzērienu un cietās plastmasas pudeles, kannas, kārbas, maisiņi, plēve, papīra<br>un kartona iepakojums, makulatūra, tetrapakas, rakstāmpapīru metāla kārbas<br>un vāciņi, skārdenes, sadzīves metāla priekšmeti',
      'li1':'<li>- netīru, slapju un<br>&nbsp;&nbsp;nesaplacinātu iepakojumu</li>'+
            '<li>- putuplastu</li>'+
            '<li>- laminētu papīru</li>'+
            '<li>- vienreizlietojamos traukus</li>',
      'li2':'<li>- plastmasas sadzīves preces</li>'+
            '<li>- gāzes balonus</li>'+
            '<li>- matu lakas un dezodorantus</li>'+
            '<li>- baterijas un akumulatorus</li>',
    },
    glass:{
      'h2':'Stikls',
      'h3':'Nedrīkst mest:',
      'p':'Stikla pudeles un burkas',
      'li1':'<li>- netīru stikla iepakojumu<br>&nbsp;&nbsp;ar pārtikas atliekām</li>'+
            '<li>- logu un automašīnu stiklu</li>'+
            '<li>- stikla traukus</li>',
      'li2':'<li>- spuldzes</li>'+
            '<li>- smaržu flakonus</li>'+
            '<li>- kosmētikas trauciņus</li>'+
            '<li>- kosmētikas spoguļus</li>',
    },
  },

  // lottery
  lottery:{
    'title':'Kas laicīgi līgumu pārslēdz, tas <dust>laimē</dust> elektro-skūteri!',
    'desc':'Ikviens, kurš pārslēdz līgumu par atkritumu apsaimniekošanu ar A/S Tīrīga no 2019. gada 5. augusta līdz 2019. gada 30. augustam, piedalās loterijā.',
    'rules':'Noteikumi',
    'prize':'Balvas - trīs elektroskūteri Xiaomi',
    'legal':'Loterijas atļaujas Nr. 5878',
  },

  // changes
  changes:{
    'title':'Kādas būs izmaiņas?',
    left:{
      'title':'Jauns <dust>tarifs</dust>',
      'now':'Šobrīd',
      'lines':[
        'Konteineru un autoparka nodrošinājums, transporta izmaksas',
        'Dalīti vākto atkritumu savākšanas sistēmas izveide, laukumu izbūve',
        'Atkritumu apsaimniekotāja izdevumi',
        'Atkritumu sagatavošana noglabāšanai, noglabāšana un Dabas resursu nodoklis (DRN)',
        'Pievienotās vērtības nodoklis (PVN)',
      ],
      'total':'KOPĀ:',
    },
    right:{
      'title':'Jauni <dust>ieguvumi</dust>',
      'list':[
        'Viena cena — vienādi nosacījumi visā pilsētā',
        'Efektīva loģistika — viens maršruts, viena iela',
        'Iespēja ietekmēt savu rēķina apmēru, šķirojot atkritumus',
        'Atkritumu šķirošanas iespējas pie mājām visā Rīgā',
        'Klusas un videi draudzīgas atkritumu savākšanas mašīnas',
        'Šķiroto atkritumu izvešana bez maksas',
      ],
    },
  },

  // Goals
  goals:{
    title:'Kāds ir Tīrīga mērķis?',
    list:[
      {title:'Tīrīga<br />domāšana.',desc:'Izglītot un aicināt ikvienu piedalīties pilsētas sakārtošanā, apzināti piedomājot pie vides tīrības ikdienā'},
      {title:'Tīrīga<br />rīcība.',desc:'Sekot pasaules piemēriem un ieviest mūsdienīgus risinājumus atkritumu apsaimniekošanas infrastruktūrā'},
      {title:'Tīrīga<br />pilsēta.',desc:'Mērķtiecīgas domāšanas un darbības rezultātā, kopīgiem spēkiem padarīt mūsu pilsētu labāku'},
    ],
  },

  // Timeline
  timeline:{
    title:'Kā mainīsim apsaimniekošanas kārtību un padarīsim vidi tīrīgu?',
    desc:'Katram mērķim nepieciešami pieturpunkti. Mūsējie ir šādi:',
    list:[
      {title:'2019. gada 15.jūlijs',desc:'Tīrīga sāk pārslēgt līgumus ar Rīgas iedzīvotājiem'},
      {title:'2019. gada 15. septembris',desc:'Tīrīga sāk sniegt pakalpojumus pilsētas iedzīvotājiem'},
      {title:'2020. gada janvāris',desc:'Jaunu atkritumu konteineru ieviešana pilsētā'},
      {title:'2020. gada aprīlis',desc:'Jauno šķiroto atkritumu savākšanas laukumu izveide'},
      {title:'2020. gada marts',desc:'Darbu uzsāk jauni, videi draudzīgi atkritumu auto'},
    ],
  },

  // middle part
  'contractSample':'Pārslēgt&nbsp;līgumu',
  'info':'Kas attiecas uz tevi?',

  's1title':'Ja tev pieder <dust>privātmāja</dust>',
  's1text':'Tev jāiepazīstas ar jauno apsaimniekošanas kārtību no apsaimniekotāja Tīrīga un jānoslēdz jaunais atkritumu apsaimniekošanas līgums.',
  's1recycling':'<li>Ja šķirojamo atkritumu apjoms ir mazs, ērti ir izmantot divu veidu šķirošanas somas stiklam un jauktajam iepakojumam (papīram, metālam, plastmasai), to saturs <b>bez maksas</b> ir iztukšojams jebkurā šķiroto atkritumu punktā vai laukumā. Turklāt klienti, kas līgumu ar a/s  “Tīrīga” par atkritumu apsaimniekošanu būs noslēguši līdz 31.augustam, somas tiks piegādātas bez maksas. Pārējie klienti somas varēs iegādāties saskaņā ar cenrādi.</li><br />'+
                '<li>Ja šķirojamo atkritumu apjoms ir lielāks, ir iespējams nomāt vai iegādāties 0,24 m3 konteinerus stiklam un jauktajam iepakojumam. Konteineru noma izmaksās 1,21 eiro mēnesī par vienu konteineru. Gan tad, ja klients lieto savā īpašaumā esošo konteineru vai to iegādājas savā īpašumā, vai to nomā, šķiroto atkritumu izvešana būs <b>bez maksas.</b></li><br />'+
                '<li>Ja vismaz piecas privātmājas vienā rajonā vienosies <u>par kopīgu šķiroto atkritumu konteineru</u> izmantošanu, tad konteiners tiks uzstādīts <b>bez maksas</b>!</li><br />'+
                '<li>Kopumā <b>visā Rīgā tiks izveidoti 2500 dalītās atkritumu savākšanas punkti.</b></li>',

  's2title':'Ja tu esi <dust>apsaimniekotājs</dust>',
  's2text':'Tev jāparūpējas par jaunā atkritumu izvešanas pakalpojuma līguma noslēgšanu, kā arī jāiepazīstas ar jaunā pakalpojuma sniedzēja Tīrīga apsaimniekošanas kārtību un jāpaziņo tā mājas iedzīvotājiem.',

  's3title':'Ja tev pieder <dust>dzīvoklis</dust> vai tu īrē dzīvokli',
  's3text':'Ja esi dzīvokļa īpašnieks vai īrē dzīvokli, Tev pašam līgums nav jāslēdz. Tavā vietā līgumu noslēgs Tavas mājas apsaimniekotājs.',
  's3recycling':'Daudzdzīvokļu namiem un publiskām ēkām (skolas, veikali, noliktavas) šķiroto sadzīves atkritumu konteineri tiks nodrošināti <b>bez maksas</b> – par velti būs pieejami konteineri stiklam (0,66 m3)  un jauktajam iepakojumam (1 m3 papīram, metālam, plastmasai). Konteineri tiks izvietoti pieejamā attālumā no celtnēm, kopumā <b>visā Rīgā tiks izveidoti 2500 dalītās atkritumu savākšanas punkti.</b>',

  's4title':'Ja tu esi <dust>juridiska persona</dust>',
  's4text':'Tev jāparūpējas par jaunā atkritumu izvešanas pakalpojuma līguma noslēgšanu, kā arī jāiepazīstas ar jaunā pakalpojuma sniedzēja Tīrīga apsaimniekošanas kārtību un jāpaziņo tā nomniekiem.',

  'btnRecyclingOptions':'Tavas&nbsp;šķirošanas&nbsp;iespējas',

  'makeDeal':'Līguma paraugs',

  // footer
  'gofooter':'Atjauno atkritumu<br />apsaimniekošanas līgumu!',

  'btnCleanR':'Clean R klientiem',
  'btnEcoBaltiaVide':'Eco Baltia Vide klientiem',
  'btnNewClients':'Jauniem klientiem',

  'footer':'Personas datu apstrāde',

  'contactsTitle':'Tīrīga AS Klientu<br />apkalpošanas centri: ',
  'contactsAdress1':'Pepsi Bowling Sporta un atpūtas centrā<br />Uzvaras&nbsp;bulvārī&nbsp;10',
  'contactsAdress2':'Rīgas domes klientu apkalpošanas centrs,<br />1. stāvs, Brīvības ielā 49/53',
  'contactsWorkdays1':'Darbadienās: 8.00 – 18.00',
  'contactsWeekend1':'Sestdienās: 10.00 – 15.00',
  'contactsWorkdays2':'Darbadienās: 8.00 – 19.00',
  'contactsWeekend2':'Sestdienās: 10.00 – 15.00',
  'contactsPhoneStr':'Tālrunis',
  'contactsPhoneNr':'1848*, 67847844',
  'contactsNotice':'Maksa par zvanu saskaņā ar operatora noteiktajiem tarifiem',
  'contactsEmailStr':'E-pasts',
  'contactsEmail':'klienti@tiriga.lv',
  'contactsComp':'”Tīrīga” AS',
  'contactsRegNr':'Reģ. Nr. 40203215318',
  'contactsLegal':'Jur. adrese: Vietalvas iela 5A, Rīga, LV-1009',
  'contactsPostal':'Pasta adrese: Vietalvas iela 5A, Rīga, LV-1009',
  'contactsBank':'Banka: A/S Luminor bank',
  'contactsBankCode':'Kods: RIKOLV2X',
  'contactsBankAcc':'Konts:LV50RIKO0002930275309',

  'footerContacts':'<strong>Kontakti medijiem:</strong><br />Elīna Dobulāne, komunikācijas konsultante<br />Mob. tālr. + 371 25959447<br />E-pasts <a href="edobulane@golin.com" target="_blank">edobulane@golin.com</a>',

  'cookies':'Mēs izmantojam sīkdatnes (cookies), lai nodrošinātu optimālu lietošanas pieredzi un tīmekļa vietnes funkcionalitāti. Nospiežot pogu "Es piekrītu", jūs paziņojat, ka piekrītat izmantot sīkdatnes saskaņā ar mūsu Privātuma politiku.',
  'cookiesAgree':'Piekrītu',

}
